export interface ISearchDropdownItem {
  name: string;
  id: string;
  selected?: boolean;
}
