import {environment} from '../../../environments/environment';

export const IDP_LINK = `${environment.authUrl}/idp/login?lvl=2&url=${environment.frontUrl}`;
