import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AuthService} from './auth.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService, // TODO: Then use auth service
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authService.isLoggedIn()) {
      request = request.clone({headers: request.headers.set('sso', this.authService.sso)});
    }

    return next.handle(request).pipe(
      catchError((err: any) => {
        switch (err.status) {
          case 401:
            this.authService.unauthorized = true;
            this.authService.logout();
            break;
          case 500:
            console.error('Server error');
            break;
          default:
            console.error(err);
        }
        if (!this.authService.isLoggedIn()) {
          this.authService.logout();
        }
        const error = err.error.message || err.statusText;
        return throwError(error);
      })
    );
  }
}
