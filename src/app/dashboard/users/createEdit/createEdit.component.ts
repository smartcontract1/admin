import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../back/services/auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Permission} from '../../../shared/types/Permission';

@Component({
  selector: 'app-users-create-edit',
  templateUrl: './createEdit.component.html',
  styleUrls: ['./createEdit.component.css']
})
export class UsersCreateEditComponent implements OnInit {

  user;
  permissions: Permission;
  newUser = {
    iin: '',
    firstName: '',
    lastName: '',
    role: 'SIMPLE'
  };
  id;
  isUserFound;

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    public router: Router
  ) {
  }

  ngOnInit() {
    this.permissions = this.authService.permissions.users;
    this.id = this.route.snapshot.params['id'];
    this.id ? this.getUser() : null;
  }

  getUser() {
    this.authService.getUser(this.id).subscribe(res => {
      this.user = res;
    });
  }

  create() {
    if (this.id) {
      this.authService.updateUser(this.user).subscribe(res => {
        this.router.navigate(['/dashboard/users/list']);
      });
    } else {
      this.authService.createUser(this.newUser).subscribe(res => {
        this.router.navigate(['/dashboard/users/list']);
      });
    }
  }

  onInputChange(event) {
    const key = event.key;
    if ((key >= 0 && key <= 9) || (key === 'Backspace' || key === 'Delete')) {
      return true;
    }
    return false;
  }

  searchUser() {
    this.authService.searchUser(this.newUser.iin)
      .subscribe(res => {
        const result = res as any;
        this.parseName(result.elements[0]);
        this.isUserFound = true;
      });
  }

  parseName(fullName) {
    const namesArr = fullName.name.ru.split(' ');
    this.newUser = {
      ...this.newUser,
      firstName: namesArr[1],
      lastName: namesArr[0],
    };
  }
}
