export const environment = {
  production: false,
  baseUrl: 'http://api.test.sc.egov.kz/v1',
  baseUrlWithoutVersion: 'http://api.test.sc.egov.kz',
  authUrl: 'https://test.idp.egov.kz',
  frontUrl: 'http://admin.test.sc.egov.kz',
  domain: '.egov.kz'
};
