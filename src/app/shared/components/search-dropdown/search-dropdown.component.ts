import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges, OnDestroy} from '@angular/core';
import {ISearchDropdownItem} from './search-dropdown-item';

@Component({
  selector: 'app-search-dropdown',
  templateUrl: './search-dropdown.component.html',
  styleUrls: ['./search-dropdown.component.css']
})
export class SearchDropdownComponent implements OnInit, OnChanges {
  // Items List:
  @Input() items: ISearchDropdownItem[] = [];
  @Input() placeholder = '';
  // Multiple or single select:
  @Input() multiple = false;
  // Has search field or not:
  @Input() search = true;
  // If multiple returns all objects, else returns selected item:
  @Output() selectItem = new EventEmitter<ISearchDropdownItem[]>();
  // Shows text, when items are empty
  @Input() emptyText = '';

  public searchText = '';
  public selectedText = '';
  public showList = false;
  public arrowState: 'up' | 'down' = 'down';

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.selectedItem) {
      const curVal = changes.selectedItem.currentValue;
      this.selectedText = curVal.name;
    }
    if (changes && changes.items && changes.items.currentValue) {
      this.items = changes.items.currentValue;
      this.setSelectedText();
    }
  }

  onShowDropdown() {
    this.showList = true;
    this.arrowState = 'up';
  }

  onCloseDropdown() {
    this.showList = false;
    this.arrowState = 'down';
  }

  onToggleDropdown() {
    !this.showList ? this.onShowDropdown() : this.onCloseDropdown();
  }

  onFocus() {
    this.onShowDropdown();
  }

  onSelectItem(item) {}

  onChangeSelected(item) {
    this.selectedText = item.name;
    if (!this.multiple) {
      this.items.map(it => {
        it.selected = it.id === item.id;
      });
      this.selectItem.emit([item]);
      this.onCloseDropdown();
    } else {
      // count selected items:
      const selected = this.setSelectedText();
      this.selectItem.emit(selected);
    }
  }
  /**
   * Returns selected items:
   */
  setSelectedText(): ISearchDropdownItem[] {
    const arr = this.items.filter((it: ISearchDropdownItem) => it.selected);
    console.log(arr);
    if (arr.length === 1) {
      this.selectedText = this.items.find(it => it.selected).name;
    } else if (arr.length === 0) {
      this.selectedText = '';
    } else {
      this.selectedText = `Выбрано: ${arr.length}`;
    }
    return arr;
  }
}
