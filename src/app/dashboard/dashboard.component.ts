import { Component, OnInit } from '@angular/core';
import { AuthService } from '../back/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  permissions = this.authService.permissions;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    if (this.authService.sso) {
      if (!this.authService.user) {
        this.getProfile();
      } else {
        this.permissions = this.authService.permissions;
      }
    } else {
      this.authService.logout();
    }
  }

  private getProfile() {
    if (this.authService.sso && !this.authService.user) {
      this.authService.getProfile().subscribe(res => {
        this.permissions = this.authService.permissions;
      }, err => {
        this.authService.logout();
      });
    }
  }

  onLogout() {
    this.authService.logout();
  }


}
