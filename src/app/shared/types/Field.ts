export enum IFieldSourceType {
  DYNAMIC = 'DYNAMIC',
  STATIC = 'STATIC',
  DYNAMIC_LIST = 'DYNAMIC_LIST',
  LIST_ITEM = 'LIST_ITEM',
}
