import {
  Component,
  AfterViewInit,
  EventEmitter,
  OnDestroy,
  Input,
  Output, OnInit
} from '@angular/core';

// import 'tinymce';
import 'tinymce/themes/modern';
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/anchor';
import 'tinymce/plugins/autolink';
import 'tinymce/plugins/autoresize';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/code';
import 'tinymce/plugins/codesample';
import 'tinymce/plugins/colorpicker';
import 'tinymce/plugins/contextmenu';
import 'tinymce/plugins/directionality';
import 'tinymce/plugins/emoticons';
import 'tinymce/plugins/fullpage';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/hr';
import 'tinymce/plugins/image';
import 'tinymce/plugins/imagetools';
import 'tinymce/plugins/importcss';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/legacyoutput';
import 'tinymce/plugins/link';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/media';
import 'tinymce/plugins/nonbreaking';
import 'tinymce/plugins/noneditable';
import 'tinymce/plugins/pagebreak';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/preview';
import 'tinymce/plugins/print';
import 'tinymce/plugins/save';
import 'tinymce/plugins/searchreplace';
import 'tinymce/plugins/spellchecker';
import 'tinymce/plugins/tabfocus';
import 'tinymce/plugins/table';
import 'tinymce/plugins/template';
import 'tinymce/plugins/textcolor';
import 'tinymce/plugins/textpattern';
import 'tinymce/plugins/wordcount';
import 'tinymce/plugins/visualblocks';
import 'tinymce/plugins/visualchars';
import {ISearchDropdownItem} from '../shared/components/search-dropdown/search-dropdown-item';
import {Permission} from '../shared/types/Permission';
import {AuthService} from '../back/services/auth.service';

declare var tinymce: any;

@Component({
  selector: 'app-tiny-editor',
  templateUrl: './tiny-editor.component.html',
  styleUrls: ['./tiny-editor.component.css'],
})
export class TinyEditorComponent implements AfterViewInit, OnDestroy, OnInit {

  field = {
    code: '',
    dataType: '',
    errorMessage: {kk: '', ru: '', en: ''},
    title: {kk: '', ru: '', en: ''},
    hint: {kk: '', ru: '', en: ''},
    placeHolder: {kk: '', ru: '', en: ''},
    order: 0,
    validator: '',
    mandatory: true,
    multiChoice: false,
    readOnly: false,
    display: false,
    source: '',
    defaultValue: '',
    sourceType: '',
    targetFields: [],
    length: null,
    additional: '',
    groupTitleKz: null,
    groupTitleRu: null,
    groupTitleEn: null,
    dateGreaterThan: null,
    dateLessThan: null,
  };

  dataTypes = [
    {
      code: 'STRING',
      name: 'Строка'
    },
    {
      code: 'TEXTAREA',
      name: 'Textarea'
    },
    {
      code: 'STRING',
      name: 'Email'
    },
    {
      code: 'TEL',
      name: 'Телефон'
    },
    {
      code: 'BOOL',
      name: 'да/нет'
    },
    {
      code: 'INTEGER',
      name: 'Целое число'
    },
    {
      code: 'DOUBLE',
      name: 'Число с плавающей точкой'
    },
    {
      code: 'LOCAL_DATE',
      name: 'Дата без времени и часового пояса'
    },
    {
      code: 'TIME',
      name: 'Время без часового пояса'
    },
    {
      code: 'DATE_TIME',
      name: 'Дата и время без часового пояса'
    },

  ];

  addEdit = false;
  index = 0;
  @Input() elementId;
  @Input() value: String;
  @Input() fields: Array<any>;
  @Input() dictionaries: Array<any>;
  @Output() onEditorContentChange = new EventEmitter();

  selectedNode;
  selectedNodeType;

  lang;
  editor;
  permissions: Permission;
  modal = false;
  editing = false;
  prevItem = {};
  css = `
    @import url('https://fonts.googleapis.com/css?family=Roboto:400,500,700&subset=cyrillic,cyrillic-ext');
    .tinymce {
      font-family: 'Roboto', sans-serif;
    }
    .field {
        display: inline-block;
        padding: 5px 10px;
        background: #1170B4;
        border-radius: 4px;
        font-size: 14px;
        color: #fff;
        border: 1px solid #ccc;
    }
    emstart {
      padding: 10px 10px 5px 10px;
      background-color:#aabbcc;
      border:1px dotted #CCCCCC;
      height:50px;
      width:100px;
      }
  `;
  public parsedDictionaries: ISearchDropdownItem[];

  constructor(
    private authService: AuthService
  ) {
  }

  ngOnInit(): void {
    this.permissions = this.authService.permissions.templates;
  }

  ngAfterViewInit() {
    this.sortFileds();
    tinymce.init({
      selector: '#' + this.elementId,
      body_class: 'tinymce',
      content_style: this.css,
      plugins: ['advlist', 'anchor', 'autolink', 'charmap', 'code', 'colorpicker', 'contextmenu', 'directionality', 'emoticons', 'fullpage', 'fullscreen', 'hr', 'imagetools', 'insertdatetime', 'legacyoutput', 'link', 'lists', 'media', 'nonbreaking', 'noneditable', 'pagebreak', 'paste', 'preview', 'print', 'save', 'searchreplace', 'tabfocus', 'table', 'template', 'textcolor', 'textpattern', 'wordcount', 'visualblocks', 'visualchars',],
      skin_url: 'assets/skins/lightgray',
      height: '340',
      language: 'ru',
      language_url: 'assets/langs/ru.js',
      formats: {
        alignleft: {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'left'},
        aligncenter: {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'center'},
        alignright: {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'right'},
        alignjustify: {selector: 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes: 'full'},
        bold: {inline: 'span', 'classes': 'bold'},
        italic: {inline: 'span', 'classes': 'italic'},
        underline: {inline: 'span', 'classes': 'underline', exact: true},
        strikethrough: {inline: 'del'},
        forecolor: {inline: 'span', classes: 'forecolor', styles: {color: '%value'}},
        hilitecolor: {inline: 'span', classes: 'hilitecolor', styles: {backgroundColor: '%value'}},
        custom_format: {block: 'h1', attributes: {title: 'Header'}, styles: {color: 'red'}}
      },
      toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | mybutton',
      extended_valid_elements: 'emstart,emend',
      custom_elements: 'emstart,emend',
      setup: editor => {
        this.editor = editor;
        let d = this.elementId;
        editor.on('keyup change', () => {
          const content = editor.getContent();
          let data = {
            content: content,
            fields: this.fields,
            lang: this.elementId,
          };
          this.onEditorContentChange.emit(data);
        });
        editor.on('click', function(e) {
          if (e.target.nodeName === 'INPUT') {
            this.selectedNodeType = e.target.attributes.name.nodeValue;
            document.getElementById(`openModalButton${d}`).click();
          }
        });
        editor.addButton('mybutton', {
          text: 'Вставить поле',
          icon: 'pluscircle',
          onclick: () => {
            document.getElementById(`openModalButton${this.elementId}`).click();
          }
        });
      }
    });
  }

  onChangeDictionary(selected) {
    this.field.source = selected.map(res => res.id).join(',');
    const selectedFull = this.dictionaries
      .filter((d) => d.dictionaryType !== 'LIST_ITEM')
      .filter(d => {
        return !!selected.find(s => s.id === d.id);
      });
    if (selectedFull.length === 1) {
      this.field.sourceType = selectedFull[0].dictionaryType;
    } else if (selectedFull.length > 1) {
      const str = selectedFull.map(s => s.dictionaryType);
      // Check for multiple sourceTypes:
      const onlyOne = str.reduce((check, el) => (check = el === check ? check : false), str[0]);
      this.field.sourceType = onlyOne ? onlyOne : 'DYNAMIC';
    } else {
      this.field.sourceType = '';
    }
  }

  insertField(filed) {
    this.modal = false;
    tinymce.activeEditor.insertContent(`
        <input class="field" type="button" name="${filed.code}" value="${filed.title[this.elementId]}"/>
      `);
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }


  sortFileds() {
    if (this.fields && this.fields.length) {
      this.fields = this.fields.sort((a: any, b: any) => {
        if (a['order'] < b['order']) {
          return -1;
        } else if (a['order'] > b['order']) {
          return 1;
        } else {
          return 0;
        }
      });
    }
  }

  openNewField() {
    this.addEdit = true;
    this.parsedDictionaries = this.dictionaries
      .filter((d) => d.dictionaryType !== 'LIST_ITEM')
      .map<ISearchDropdownItem>(
      (d: any) => ({id: d.id, name: d.title[this.elementId], selected: false})
    );
  }

  addNewField() {
    if (!this.field.title.ru || !this.field.code) {
      return false;
    }
    !this.fields ? this.fields = [] : null;
    // this.field.order = this.fields.length;

    if (this.editing) {
      this.fields[this.index] = this.field;
      const old = `<input class="field" name="${this.prevItem['code']}" type="button" value="${this.prevItem['title'][this.elementId]}" />`;
      const last = `<input class="field" name="${this.field['code']}" type="button" value="${this.field['title'].ru}" />`;
      const re = new RegExp(old, 'g');
      this.value = this.value.replace(re, last);
      tinymce.activeEditor.setContent(this.value);
    } else {
      this.fields.push(this.field);
    }

    this.sortFileds();
    this.closeAddEdit();
  }

  closeAddEdit() {
    this.addEdit = false;
    this.field = {
      code: '',
      dataType: '',
      errorMessage: {kk: '', ru: '', en: ''},
      title: {kk: '', ru: '', en: ''},
      hint: {kk: '', ru: '', en: ''},
      placeHolder: {kk: '', ru: '', en: ''},
      order: 0,
      validator: '',
      mandatory: true,
      multiChoice: false,
      readOnly: false,
      display: false,
      source: '',
      defaultValue: '',
      sourceType: '',
      targetFields: [],
      length: null,
      additional: '',
      groupTitleKz: null,
      groupTitleRu: null,
      groupTitleEn: null,
      dateGreaterThan: null,
      dateLessThan: null,
    };
    this.editing = false;
    this.parsedDictionaries = [];
  }

  edit(index, item) {
    this.scrollTop();
    for (const elem in this.field) {
      if (item[elem]) {
        const data = item[elem];
        this.field[elem] = data;
      }
    }
    const selectedIds: string[] = this.field.source.split(',');
    this.parsedDictionaries = this.dictionaries
      .filter((d) => d.dictionaryType !== 'LIST_ITEM')
      .map<ISearchDropdownItem>(
      (d: any) => ({id: d.id, name: d.title[this.elementId], selected: false})
    );
    selectedIds.map(si => {
      this.parsedDictionaries.map((pd: ISearchDropdownItem) => {
        if (pd.id === si) {
          pd.selected = true;
        }
      });
    });
    this.prevItem = JSON.parse(JSON.stringify(this.field));
    this.index = index;
    this.editing = true;
    this.addEdit = true;
  }

  delete(index, item) {
    this.fields.splice(index, 1);
    const input = `<input class="field" name="${item.code}" type="button" value="${item.title.ru}" />`;
    const re = new RegExp(input, 'g');
    this.value = this.value.replace(re, '');
    tinymce.activeEditor.setContent(this.value);
  }

  scrollTop() {
    const myDiv = document.getElementById('modalBody');
    myDiv.scrollTop = 0;
  }
}
