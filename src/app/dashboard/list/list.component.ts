import { Component, OnInit } from '@angular/core';
import { AppletService } from '../../back/services/applet.service';
import { AuthService } from '../../back/services/auth.service';
import {Permission} from '../../shared/types/Permission';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  list = [];
  permissions: Permission;
  lang = 'ru';
  state =  'ALL';
  entity = 'ALL';
  deleteDoc;
  modal = false;
  loading: boolean;
  constructor(
    public authService: AuthService,
    public app: AppletService
  ) { }

  ngOnInit() {
    this.permissions = this.authService.permissions.templates;
    this.getList();
  }

  getList() {
    this.loading = true;
    const filters = {
      start: 0,
      limit: 150,
      state: this.state,
      ...(this.entity !== 'ALL' && {entity: this.entity})
    };
    this.app.getDocuments(filters).subscribe(res => {
      res.sort((a: any, b: any) => {
            if (a.createdDate > b.createdDate) {
              return -1;
            } else if (a.createdDate < b.createdDate) {
              return 1;
            } else {
              return 0;
            }
          });
      this.list = res;
      this.loading = false;
    }, () => this.loading = false);
  }

  approve(id) {
    // this.app.getDocumentById(id).subscribe(res => {
    //   console.log(res);
    // });
    this.app.approveDocument(id).subscribe(res => {
      this.getList();
    });
  }

  deleteTemplate(doc) {
    this.deleteDoc = doc;
    this.modal = true;
  }

  deleteModalTemplate(id) {
    this.app.deleteTemplate(id).subscribe(res => {
      this.modal = false;
      this.getList();
    });
  }


}
