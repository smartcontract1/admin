import { Permission } from './Permission';

export interface PermissionList {
  users: Permission;
  dictionaries: Permission;
  templates: Permission;
}
