export const userPermissions = {
  ADMIN: {
    users: {
      canCreate: true,
      canEdit: true,
      canDelete: true,
      canRead: true
    },
    dictionaries: {
      canCreate: true,
      canEdit: true,
      canDelete: true,
      canRead: true
    },
    templates: {
      canCreate: true,
      canEdit: true,
      canDelete: true,
      canRead: true,
      canApprove: true
    }
  },
  MANAGER: {
    users: {
      canCreate: false,
      canEdit: false,
      canDelete: false,
      canRead: false
    },
    dictionaries: {
      canCreate: false,
      canEdit: true,
      canDelete: false,
      canRead: true
    },
    templates: {
      canCreate: false,
      canEdit: true,
      canDelete: false,
      canRead: true,
      canApprove: false
    }
  },
  SIMPLE: {
    users: {
      canCreate: false,
      canEdit: false,
      canDelete: false,
      canRead: false
    },
    dictionaries: {
      canCreate: false,
      canEdit: false,
      canDelete: false,
      canRead: true
    },
    templates: {
      canCreate: false,
      canEdit: false,
      canDelete: false,
      canRead: true,
      canApprove: false
    }
  }
};
