import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppletService } from '../../back/services/applet.service';
import { Permission } from '../../shared/types/Permission';
import { AuthService } from '../../back/services/auth.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})

export class CreateComponent implements OnInit {
  editor = { ru: null, kk: null, en: null };
  modal = false;
  id;
  permissions: Permission;
  document = {
    templateId: '',
    title : {
      ru : '',
      kk : '',
      en : ''
    },
    description: {
      ru : '',
      kk : '',
      en : ''
    },
    inputForm : {
      fields : {
        1 : []
      }
    },
    entity: 'INDIVIDUAL',
    documentOwner: []
  };
  template;
  lang = 'kk';
  fields;
  dictionaries;
  bins: string[] = [];
  accessAll: boolean;

  constructor(
    private route: ActivatedRoute,
    private app: AppletService,
    private router: Router,
    private authService: AuthService
  ) {
    this.getDictionaries();
  }

  ngOnInit() {
    this.permissions = this.authService.permissions.templates;
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.getDocument();
    } else {
      this.empty();
    }
  }

  empty() {
    this.editor = { ru: ' ', kk: ' ', en: ' ' };
  }

  getDictionaries() {
    this.app.getDictionaries().subscribe(res => {
      this.dictionaries = res;
    });
  }

  getDocument() {
    this.app.getDocumentById(this.id).subscribe(res => {
      this.document = res;
      this.bins = this.document.documentOwner;
      if (this.document.inputForm) {
        this.fields = this.document.inputForm.fields[1];
      }
      this.getTemplate(res.templateId);
    });
  }

  getTemplate(id) {
    this.app.getDocumentTemplate(id).subscribe(res => {
      this.template = res;
      if (this.template.localizedTemplates.ru) {
        this.editor.ru = this.template.localizedTemplates.ru.body;
      }
      if (this.template.localizedTemplates.kk) {
        this.editor.kk = this.template.localizedTemplates.kk.body;
      }
      if (this.template.localizedTemplates.en) {
        this.editor.en = this.template.localizedTemplates.en.body;
      }
      if (this.fields) {
        this.fields.map(item => {
          if (this.editor.ru) {
            const inputRu = `<input class="field" name="${item.code}" type="button" value="${item.title.ru}" />`;
            const stringRu = '{{' + item.code + '}}';
            this.editor.ru = this.replaceGlobally(this.editor.ru, stringRu, inputRu);
          }
          if (this.editor.kk) {
            const inputKk = `<input class="field" name="${item.code}" type="button" value="${item.title.kk}" />`;
            const stringKk = '{{' + item.code + '}}';
            this.editor.kk = this.replaceGlobally(this.editor.kk, stringKk, inputKk);
          }
          if (this.editor.en) {
            const inputEn = `<input class="field" name="${item.code}" type="button" value="${item.title.en}" />`;
            const stringEn = '{{' + item.code + '}}';
            this.editor.en = this.replaceGlobally(this.editor.en, stringEn, inputEn);
          }
        });
      }
    });
  }

  replaceGlobally(original, searchTxt, replaceTxt) {
      const regex = new RegExp(this.escapeRegExp(searchTxt), 'g');
      return original.replace(regex, replaceTxt);
  }

  escapeRegExp(str) {
      return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  keyupHandler(event) {
    const lang = event.lang;
    this.editor[lang] = event.content;
    this.fields = event.fields;
  }

  create() {
    const data = {
      title: this.document.title,
      description: this.document.description,
      templateId: this.document.templateId,
      entity: this.document.entity,
      inputForm : {
        fields : {
          1 : []
        }
      },
      documentOwner: this.accessAll ? [] : this.bins,
    };
    console.log(data);
    const content = {ru: this.editor.ru || '', en: this.editor.en || '', kk: this.editor.kk || ''};
    if (this.fields) {
      this.fields.map(item => {
        const inputRU = `<input class="field" name="${item.code}" type="button" value="${item.title.ru}" />`;
        const re = new RegExp(inputRU, 'g');
        content.ru = content.ru.replace(re, '{{' + item.code + '}}');

        const inputKK = `<input class="field" name="${item.code}" type="button" value="${item.title.kk}" />`;
        const re2 = new RegExp(inputKK, 'g');
        content.kk = content.kk.replace(re2, '{{' + item.code + '}}');

        const inputEN = `<input class="field" name="${item.code}" type="button" value="${item.title.en}" />`;
        const re3 = new RegExp(inputEN, 'g');
        content.en = content.en.replace(re3, '{{' + item.code + '}}');
      });
    }

    if (this.id) {
      data.inputForm.fields['1'] = this.fields;
      this.template.localizedTemplates.ru.body = content.ru;
      this.template.localizedTemplates.kk.body = content.kk;
      this.template.localizedTemplates.en = {
        body: content.en,
        description: '',
        locale: 'en',
        title: '',
      };
      this.app.updateTemplate(data.templateId, this.template).subscribe(res => {
        this.app.updateDocumentInfo(this.id, data).subscribe(val => {
          this.router.navigate(['/dashboard/list/']);
        });
      });
    } else {
      this.createTemplate(content, data);
    }
  }

  createTemplate(content, document) {
    const data = {
      localizedTemplates: {
        ru: {
          title: this.document.title.ru,
          locale: 'ru',
          description: this.document.description.ru,
          body: content.ru,
        },
        kk: {
          title: this.document.title.kk,
          locale: 'kk',
          description: this.document.description.kk,
          body: content.kk,
        },
        en: {
          title: this.document.title.kk,
          locale: 'kk',
          description: this.document.description.kk,
          body: content.kk,
        }
      }
    };
    this.app.createTemplate(data).subscribe(res => {
      document.templateId = res.id;
      document.inputForm.fields['1'] = this.fields;
      this.app.createDocument(document).subscribe(val => {
        this.router.navigate(['/dashboard/list/']);
      });
    });
  }

  addNewBin() {
    console.log(this.bins);
    if (!this.accessAll) {
      this.bins.push('');
    }
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }
}
