import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../back/services/auth.service';
import {Permission} from '../../../shared/types/Permission';


@Component({
  selector: 'app-user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class UserListComponent implements OnInit {
  list = [];
  permissions: Permission;
  modal = false;
  selectedIIN;

  constructor(public authService: AuthService) {
  }

  ngOnInit() {
    this.permissions = this.authService.permissions.users;
    this.getList();
  }

  getList() {
    this.authService.getUsers().subscribe((res: any[]) => {
      this.list = res;
    });
  }

  deleteUser(iin) {
    this.selectedIIN = iin;
    this.modal = true;
  }

  deleteModalUser() {
    this.authService.deleteUser(this.selectedIIN).subscribe(res => {
      this.getList();
      this.modal = false;
    }, error => {
      if (error.status === 200) {
        this.getList();
        this.modal = false;
      }
    });
  }

}
