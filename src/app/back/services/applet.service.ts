import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppletService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
  }

  getDictionaries() {
    return this.http.get(`${environment.baseUrl}/dictionary`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getDictionary(id): Observable<any> {
    return this.http.get(`${environment.baseUrl}/dictionary/${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  createDictionary(data): Observable<any> {
    return this.http.post(`${environment.baseUrl}/dictionary/`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  addElements(url, data): Observable<any> {
    return this.http.post(environment.baseUrl + url, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  updateDictionary(data): Observable<any> {
    return this.http.put(`${environment.baseUrl}/dictionary`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  deleteDictionary(id): Observable<any> {
    return this.http.delete(`${environment.baseUrl}/dictionary/${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getDocuments(params): Observable<any> {
    return this.http.get(`${environment.baseUrl}/admin/documents/read`, {params}).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getDocumentById(id): Observable<any> {
    return this.http.get(`${environment.baseUrl}/admin/documents/${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  updateDocumentInfo(id, data): Observable<any> {
    return this.http.put(`${environment.baseUrl}/documents/${id}`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  updateTemplate(id, data): Observable<any> {
    return this.http.put(`${environment.baseUrl}/documents/templates/${id}`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getDocumentTemplate(id): Observable<any> {
    return this.http.get(`${environment.baseUrl}/documents/templates/${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  createTemplate(data): Observable<any> {
    return this.http.post(`${environment.baseUrl}/documents/templates`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  createDocument(data): Observable<any> {
    return this.http.post(`${environment.baseUrl}/documents/`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  approveDocument(id): Observable<any> {
    return this.http.post(`${environment.baseUrl}/documents/${id}/approve`, {}).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  deleteTemplate(id): Observable<any> {
    return this.http.delete(`${environment.baseUrl}/documents/${id}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    return throwError(error);
  }

}
