import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchDropdownComponent} from './components/search-dropdown/search-dropdown.component';
import {ClickOutsideDirective} from './directives/click-outside.directive';
import {FormsModule} from '@angular/forms';
import {SearchPipe} from './pipes/search.pipe';

@NgModule({
  declarations: [
    SearchDropdownComponent,
    ClickOutsideDirective,
    SearchPipe
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    SearchDropdownComponent,
  ]
})
export class SharedModule { }
