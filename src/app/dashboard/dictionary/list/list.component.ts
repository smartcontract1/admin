import { Component, OnInit } from '@angular/core';
import { AppletService } from '../../../back/services/applet.service';
import { AuthService } from '../../../back/services/auth.service';
import {Permission} from '../../../shared/types/Permission';

@Component({
  selector: 'app-dictionary-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class DictionaryListComponent implements OnInit {

  list;
  permissions: Permission;

  constructor(
    public app: AppletService,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.permissions = this.authService.permissions.dictionaries;
    this.getList();
  }

  getList() {
    this.app.getDictionaries().subscribe(res => {
      this.list = res;
    });
  }

  deleteDictionary(id){
    this.app.deleteDictionary(id).subscribe(res => {
      this.getList();
    }, error => {
      if (error.status === 200) {
        this.getList();
      }
    });
  }

}
