export const mapDOM  = (element, json) => {
      var treeObject = {};
      // If string convert to document Node
      if (typeof element === "string") {
          if (window.DOMParser) {
                var parser = new DOMParser();
                var docNode = parser.parseFromString(element,"text/xml");
          } else { // Microsoft strikes again
                docNode = new ActiveXObject("Microsoft.XMLDOM");
                docNode.async = false;
                docNode.loadXML(element); 
          } 
          element = docNode.firstChild;
      }

      //Recursively loop through DOM elements and assign properties to object
      function treeHTML(element, object) {
          // object["type"] = element.nodeName;
          var nodeList = element.childNodes;
          if (nodeList != null) {
              if (nodeList.length) {
                  object["nodes"] = [];
                  for (var i = 0; i < nodeList.length; i++) {
                    var obj = {};
                    obj['link'] = nodeList[i].childNodes[0].attributes['href'].nodeValue;
                    obj['label'] = nodeList[i].childNodes[0].childNodes[0].nodeValue;
                    var curUl = {};
                    obj["nodes"] = curUl;
                    if (nodeList[i].childNodes.length > 1) {
                      treeHTML(nodeList[i].childNodes[1], curUl);
                      obj["nodes"] = curUl["nodes"];
                    }
                    
                    object["nodes"].push(obj);
                  }
              }
          }
      }
      treeHTML(element, treeObject);
      return (json) ? JSON.stringify(treeObject) : treeObject;
  }