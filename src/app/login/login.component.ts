import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../back/services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {IDP_LINK} from '../shared/constants/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() minHeight = '100vh';

  public loading = false;
  public user = {
    username: '',
    password: ''
  };
  public idpLink = IDP_LINK;

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
  }

  onLogin() {
    this.loading = true;
    this.authService.login(this.user).subscribe(res => {
      this.loading = false;
      if (this.router.url.includes('login')) {
        this.router.navigate(['/dashboard']);
      }
      },
      error => {
      this.loading = false;
      this.toastr.error('Логин или пароль был введен не корректно! Попробуйте заново.');
      });
  }

  disabled() {
    return this.user.username.length === 0 || this.user.password.length === 0 || this.loading;
  }
}
