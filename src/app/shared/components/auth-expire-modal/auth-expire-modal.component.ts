import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../back/services/auth.service';

@Component({
  selector: 'app-auth-expire-modal',
  templateUrl: './auth-expire-modal.component.html',
  styleUrls: ['./auth-expire-modal.component.css']
})
export class AuthExpireModalComponent implements OnInit {

  public timeIsOver;
  constructor(
    public authService: AuthService
  ) {
  }

  ngOnInit() {
    // this.countTimer();
  }

  countTimer() {
    setInterval(() => {
      if (this.authService.isLoggedIn()) {
        this.timeIsOver = (((new Date()).getTime() - this.authService.startTime) / 1000) >= 1800;
      }
    }, 1000);
  }

}
