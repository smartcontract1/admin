import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkLogin(route, state.url);
  }

  checkLogin(route: any, url: string): boolean {
    if (this.authService.isLoggedIn()) {
      if (route.url[0].path.includes('login')) {
        this.router.navigate(['/dashboard']);
        return false;
      }
      return true;
    }
    this.authService.redirectUrl = url;
    if (route.url[0].path.includes('login')) {
      return true;
    }
    this.router.navigate(['/login']);
    return true;
  }
}
