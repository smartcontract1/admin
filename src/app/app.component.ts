import {Component, OnInit} from '@angular/core';
import {AuthService} from './back/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'smartcontract-admin';
  constructor(
    private authService: AuthService,
  ) {}

  ngOnInit(): void {}
}
