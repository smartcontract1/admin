import {BrowserModule} from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AuthGuard} from './back/services/auth.guard';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ListComponent} from './dashboard/list/list.component';
import {UserListComponent} from './dashboard/users/list/list.component';
import {CreateComponent} from './dashboard/create-edit/create.component';
import {TinyEditorComponent} from './tiny-editor/tiny-editor.component';
import {AppletService} from './back/services/applet.service';
import {AuthService} from './back/services/auth.service';
import {registerLocaleData} from '@angular/common/';
import ru from '@angular/common/locales/ru';

import {AuthExpireModalComponent} from './shared/components/auth-expire-modal/auth-expire-modal.component';
import {DictionaryListComponent} from './dashboard/dictionary/list/list.component';
import {DictionaryCreateEditComponent} from './dashboard/dictionary/createEdit/createEdit.component';
import {UsersCreateEditComponent} from './dashboard/users/createEdit/createEdit.component';
import {HttpConfigInterceptor} from './back/services/HttpConfig.interceptor';
import {SharedModule} from './shared/shared.module';
import {AngularFontAwesomeModule} from 'angular-font-awesome';

registerLocaleData(ru);

const appRoutes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full', canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent, canActivate: [AuthGuard]},
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', redirectTo: 'list', pathMatch: 'full'},
      {path: 'list', component: ListComponent},
      {path: 'create', component: CreateComponent},
      {path: 'edit/:id', component: CreateComponent},
      {
        path: 'users',
        children: [
          {path: '', redirectTo: 'list', pathMatch: 'full'},
          {path: 'list', component: UserListComponent},
          {path: 'edit/:id', component: UsersCreateEditComponent},
          {path: 'create', component: UsersCreateEditComponent},
        ]
      },
      {
        path: 'dictionary',
        children: [
          {path: '', redirectTo: 'list', pathMatch: 'full'},
          {path: 'list', component: DictionaryListComponent},
          {path: 'edit/:id', component: DictionaryCreateEditComponent},
          {path: 'create', component: DictionaryCreateEditComponent},
        ]
      }
    ]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ListComponent,
    CreateComponent,
    TinyEditorComponent,
    UserListComponent,
    UsersCreateEditComponent,
    AuthExpireModalComponent,
    DictionaryListComponent,
    DictionaryCreateEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    RouterModule.forRoot(appRoutes),
    SharedModule
  ],
  providers: [
    AppletService,
    AuthService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: LOCALE_ID, useValue: 'ru'},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [AuthExpireModalComponent]
})
export class AppModule {
}
