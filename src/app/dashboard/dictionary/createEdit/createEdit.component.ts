import {Component, OnInit} from '@angular/core';
import {AppletService} from '../../../back/services/applet.service';
import {Router, ActivatedRoute} from '@angular/router';
import {IFieldSourceType} from '../../../shared/types/Field';
import {Permission} from '../../../shared/types/Permission';
import {AuthService} from '../../../back/services/auth.service';

const empty = {
  id: '',
  title: {
    ru: '',
    kk: '',
    en: '',
  },
  sourceUrl: '',
  dictionaryType: 'STATIC',
  elements: [
    {
      id: 1,
      name: {
        en: '',
        ru: '',
        kk: ''
      }
    }
  ],
};

@Component({
  selector: 'app-dictionary-create-edit',
  templateUrl: './createEdit.component.html',
  styleUrls: ['./createEdit.component.css']
})
export class DictionaryCreateEditComponent implements OnInit {
  public dictionary = empty;
  public id;
  public permissions: Permission;
  public sourceTypes = IFieldSourceType;
  constructor(
    public app: AppletService,
    private route: ActivatedRoute,
    public router: Router,
    private atuhService: AuthService
  ) {}

  ngOnInit() {
    this.permissions = this.atuhService.permissions.dictionaries;
    this.id = this.route.snapshot.params.id;
    if (this.id) {
      this.getDictionary();
    }
  }

  getDictionary() {
    this.app.getDictionary(this.id).subscribe((res: any) => {
      this.dictionary = {
        ...res,
        elements: res.elements.sort((obj1, obj2) => obj1.id - obj2.id),
      };
    });
  }

  create() {
    const action = this.id ? 'updateDictionary' : 'createDictionary';
    this.app[action](this.dictionary).subscribe(res => {
        this.router.navigate(['/dashboard/dictionary/list']);
    });
  }

  deleteItem(index, item) {
    if (this.dictionary.elements.length === 1) {
      this.dictionary.elements[0] = {
        id: 1,
        name: {
          en: '',
          ru: '',
          kk: ''
        }
      };
    } else {
      this.dictionary.elements.splice(index, 1);
    }
  }

  addItem(index) {
    const data = {
      id: this.dictionary.elements.length + 1,
      name: {
        en: '',
        ru: '',
        kk: ''
      }
    };
    this.dictionary.elements.splice(index + 1, 0, data);
  }

}
