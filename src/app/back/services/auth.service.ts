import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {retry, catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {userPermissions} from '../../shared/constants/userPermissions';
import {PermissionList} from '../../shared/types/PermissionList';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public unauthorized: boolean;
  public redirectUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
  }

  getUsers() {
    return this.http.get(`${environment.baseUrlWithoutVersion}/users/`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  getUser(iin) {
    return this.http.get(`${environment.baseUrlWithoutVersion}/user/` + iin).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  updateUser(data): Observable<any> {
    return this.http.put(`${environment.baseUrlWithoutVersion}/user`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  searchUser(iin) {
    return this.http.get(`${environment.baseUrl}/dictionary/fullName/${iin}/`);
  }

  deleteUser(iin): Observable<any> {
    return this.http.delete(`${environment.baseUrlWithoutVersion}/user/${iin}`).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  createUser(data): Observable<any> {
    return this.http.post(`${environment.baseUrlWithoutVersion}/user`, data).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  login(data): Observable<any> {
    return this.http.post(`${environment.baseUrlWithoutVersion}/user/login/admin`, data)
      .pipe(
        tap((res: any) => {
          if (res.message === 'SUCCESS') {
            this.unauthorized = false;
            // this.startTime = new Date().getTime();
            this.saveSSO(res.ticket);
            this.saveUser(res.user);
          }
        }),
        catchError(this.handleError)
      );
  }
  saveSSO(sso: string) {
    this.setCookie('SSO', sso, {'max-age': 1800});
  }
  saveUser(user) {
    console.log(user)
    this.savePermissions(userPermissions[user.role]);
    localStorage.setItem('user', JSON.stringify(user));
  }

  savePermissions(permissions) {
    console.log(permissions);
    if (permissions) {
      localStorage.setItem('permissions', JSON.stringify(permissions));
    }
  }

  get permissions(): PermissionList {
    return localStorage.getItem('permissions') && JSON.parse(localStorage.getItem('permissions')) || {};
  }

  set startTime(time) {
    localStorage.setItem('startTime', JSON.stringify(time));
  }

  get startTime() {
    return localStorage.getItem('startTime') && JSON.parse(localStorage.getItem('startTime'));
  }

  get user() {
    return JSON.parse(localStorage.getItem('user'));
  }

  get sso() {
    return this.getCookie('SSO');
  }

  isLoggedIn() {
    return !!this.sso;
  }
  /**
   *     Gets profile by sso, when user authorized from idp.egov.kz
   */
  getProfile(): Observable<any> {
    // then remove:
    // this.setCookie('SSO', 'ba75f8d8-28c2-4259-804b-63a6b988b82e', {'max-age': 1800});
    // const apiUrl = 'http://api.test.sc.egov.kz/';
    return this.http.get(`${environment.baseUrlWithoutVersion}/user_profile`, {withCredentials: true})
      .pipe(tap((res: any) => {
        console.log(res);
        if (res) {
          // this.startTime = new Date().getTime();
          this.saveUser(res);
        }
      }));
  }
  getToken(name) {
    return this.getCookie('SSO');
  }
  getCookie(name: string) {
    const matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }
  setCookie(name, value, options: any = {}) {
    options = {
      path: '/',
      ...options
    };
    if (options.expires && options.expires.toUTCString) {
      options.expires = options.expires.toUTCString();
    }
    let updatedCookie = encodeURIComponent(name) + '=' + encodeURIComponent(value);
    for (const optionKey in options) {
      updatedCookie += '; ' + optionKey;
      const optionValue = options[optionKey];
      if (optionValue !== true) {
        updatedCookie += '=' + optionValue;
      }
    }
    console.log(updatedCookie);
    document.cookie = updatedCookie;
    console.log(document.cookie);
  }
  deleteCookie(name) {
    console.log('logout');
    this.setCookie(name, '', {'max-age': -1});
  }
  logout() {
    this.unauthorized = true;
    document.cookie = `SSO=; domain=${environment.domain}; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/`;
    // document.cookie = `SSO=; domain=localhost; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/`;
    localStorage.removeItem('permissions');
    localStorage.removeItem('user');
    this.router.navigate(['login']);
    // location.href = this.idpLink;
  }

  handleError(error) {
    if (error && error.status) {
      console.log(error.status);
      if (error.status === 401) {
        this.logout();
      }
    }
    return throwError(error);
  }
}
