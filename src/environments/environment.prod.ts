export const environment = {
  production: true,
  baseUrl: 'https://api.sc.egov.kz/v1',
  baseUrlWithoutVersion: 'https://api.sc.egov.kz',
  authUrl: 'https://idp.egov.kz',
  frontUrl: 'https://admin.sc.egov.kz',
  domain: '.egov.kz'
};
